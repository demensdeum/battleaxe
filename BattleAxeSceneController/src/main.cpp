#include <iostream>

using namespace std;

int main (int argc, char *argv[]) {

	cout << "Welcome to Battle Axe Scene Controller!\nBattle Axe Scene Controller - part of Flame Steel Engine Game Toolkit that allows you to use Rise scripts with intergated game toolkit dependencies." << endl;

	if (argc < 2) {

		cout << "Usage: BattleAxeSceneController -runRiseScript \"show Flame Steel Engine Game Toolkit Hello World Example\"" << endl;

	}

} 